import React from 'react';
import { Image } from 'react-native';

const Img = {
  howtoHello: <Image source={require('../../assets/images/mobile/howto-1.png')} />,
  howtoToday: <Image source={require('../../assets/images/mobile/howto-2.png')} />,
  howtoDelete: <Image source={require('../../assets/images/mobile/howto-3.png')} />,
  howtoSorting: <Image source={require('../../assets/images/mobile/howto-4.png')} />,
  howtoEdit: <Image source={require('../../assets/images/mobile/howto-5.png')} />,
};

export default Img;
