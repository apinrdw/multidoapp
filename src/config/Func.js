const Func = {
  formatDate(date) {
    if (date === undefined) return null;
    const mm = date.getMonth() + 1;
    const dd = date.getDate();
    return [date.getFullYear(), (mm > 9 ? '' : '0') + mm, (dd > 9 ? '' : '0') + dd].join('-');
  },
  getToday() {
    const today = new Date();
    return this.formatDate(today);
  },
  getDay(shift, date = 'today') {
    const fromday = (date === 'today') ? new Date() : new Date(date);
    const newdate = new Date(fromday.setDate(fromday.getDate() + shift));
    return this.formatDate(newdate);
  },
  getDayDiffAbs(thisDate, nextDate) {
    const date1 = new Date(thisDate);
    const date2 = new Date(nextDate);
    const timeDiff = Math.abs(date2.getTime() - date1.getTime());
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
  },
  getDayDiff(thisDate, nextDate) {
    const date1 = new Date(thisDate);
    const date2 = new Date(nextDate);
    const timeDiff = date2.getTime() - date1.getTime();
    const diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    return diffDays;
  },
  todayIsValid(date) {
    const d = date.split('-');
    const year = parseInt(d[0]);
    console.log(date, year);
    return year > 2000;
  },
  objEquivalent(a, b) {
    let equivalent = true;

    if (a === undefined && b === undefined) { return true; }
    if (a === null && b === null) { return true; }
    if (a === null && b === undefined) { return true; }
    if (a === undefined && b === null) { return true; }

    if (a === undefined) { return false; }
    if (a === null) { return false; }
    if (b === undefined) { return false; }
    if (b === null) { return false; }

    const aProps = Object.getOwnPropertyNames(a);
    const bProps = Object.getOwnPropertyNames(b);

    if (aProps.length !== bProps.length) { return false; }

    Object.keys(a).forEach((key) => {
      const aObj = a[key]; const bObj = b[key];
      Object.keys(aObj).forEach((k) => {
        const aData = aObj[k]; const bData = bObj[k];
        if (aData !== bData) { equivalent = false; }
      });
    });

    return equivalent;
  },
  datasAvailable(current, next) {
    if (current === null) { return false; }
    if (next === null) { return false; }
    return true;
  },
  dayAvailable(data, day) {
    if (day === undefined) { return false; }
    if (day === null) { return false; }
    if (data === undefined) { return false; }
    if (data === null) { return false; }
    if (data[day] === undefined) { return false; }
    if (data[day] === null) { return false; }
    return true;
  },

memorySizeOf(obj) {
      var bytes = 0;

      function sizeOf(obj) {
          if(obj !== null && obj !== undefined) {
              switch(typeof obj) {
              case 'number':
                  bytes += 8;
                  break;
              case 'string':
                  bytes += obj.length * 2;
                  break;
              case 'boolean':
                  bytes += 4;
                  break;
              case 'object':
                  var objClass = Object.prototype.toString.call(obj).slice(8, -1);
                  if(objClass === 'Object' || objClass === 'Array') {
                      for(var key in obj) {
                          if(!obj.hasOwnProperty(key)) continue;
                          sizeOf(obj[key]);
                      }
                  } else bytes += obj.toString().length * 2;
                  break;
              }
          }
          return bytes;
      };

      function formatByteSize(bytes) {
          if(bytes < 1024) return bytes + " bytes";
          else if(bytes < 1048576) return(bytes / 1024).toFixed(3) + " KiB";
          else if(bytes < 1073741824) return(bytes / 1048576).toFixed(3) + " MiB";
          else return(bytes / 1073741824).toFixed(3) + " GiB";
      };

      return formatByteSize(sizeOf(obj));
  }


};

export { Func };
