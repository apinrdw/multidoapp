import React from 'react';
import { Image } from 'react-native';

const Img = {
  howtoHello: <Image source={{ uri: './assets/images/desktop/howto-1.png' }} style={{ width: 280, height: 280 }} key="howtoHello" />,
  howtoDeleteEdit: <Image source={{ uri: './assets/images/desktop/howto-2.png' }} style={{ width: 280, height: 280 }} key="howtoDeleteEdit" />,
  howtoQuickEdit: <Image source={{ uri: './assets/images/desktop/howto-3.png' }} style={{ width: 280, height: 280 }} key="howtoQuickEdit" />,
  howtoSortingMoving: <Image source={{ uri: './assets/images/desktop/howto-4.png' }} style={{ width: 280, height: 280 }} key="howtoSortingMoving" />,
};

export { Img };
