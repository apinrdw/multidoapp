import { Platform } from 'react-native';
import $ from 'jquery';

const Vstd = {
  mobile: 'mobile',
  desktop: 'desktop',
  web: 'web',

  platform() {
    if (Platform.OS === 'web') {
      if ($('#AppContainer').attr('data-apptype') === 'desktop') {
        return this.desktop;
      }
      return this.web;
    }
    return this.mobile;
  },

  check(visited, setVisited) {
    if (visited === null) {
      setVisited(this.platform());
      return false;
    } else if (visited[this.platform()] === undefined) {
      setVisited(this.platform());
      return false;
    }

    return true;
  }
};

export { Vstd };
