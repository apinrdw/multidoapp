import React from 'react';
import { Platform, Dimensions } from 'react-native';

const window = Dimensions.get('window');

const Def = {
  colorBlue: (Platform.OS === 'web') ? 'rgba(29, 170, 234, 1)' : 'rgba(78, 165, 237, 1)',
  colorBlueTransparent: (Platform.OS === 'web') ? 'rgba(29, 170, 234, 0.8)' : 'rgba(78, 165, 237, 0.8)',
  colorRed: (Platform.OS === 'web') ? 'rgba(233, 28, 80, 1)' : 'rgba(221, 39, 81, 1)',
  colorInfo: (Platform.OS === 'web') ? 'rgba(24, 148, 216, 1)' : 'rgba(70, 143, 219, 1)',
  colorBlack: '#000000',
  colorWhite: '#FFFFFF',
  colorGray: '#F2F2F2',
  colorTextGray: '#828282',
  colorIcon: (Platform.OS === 'web') ? '#D9D9D9' : '#E0E0E0',
  fontFamily: 'Work Sans',
  fontRegular: 'WorkSans-Regular',
  fontLight: 'WorkSans-Light',
  fontExtLight: 'WorkSans-ExtraLight',
  fontMedium: 'WorkSans-Medium',
  weightExtraLight: '200',
  weightLight: '300',
  weightRegular: '400',
  weightMedium: '500',
  buttonHeight: 50,
  inputHeight: 50,
  wideDaysWidth: 400,
  menubarWidth: 300,
  listDuration: 600,
  jumpDuration: 400,
  modalDuration: 500,
  stageWidth: window.width,
  stageHeight: window.height,
  wide: (window.width > 500),
  menuPaddingTop: (Platform.OS === 'web') ? 50 : ((window.width > 500) ? 40 : 30),
  paddingTop: (Platform.OS === 'web') ? 80 : ((window.width > 500) ? 70 : 40),
  paddingBottom: (window.width > 500) ? 50 : 50,
  Br: (Platform.OS === 'web') ? <br /> : '\n',
  taskHeight: 43,
  taskTextSize: 15,
  taskTextPaddingTop: (Platform.OS === 'android') ? 2 : 0,
  taskPaddingLeft: 15,
  taskPaddingRight: 15,
  taskMarginBottom: 10,
  settings: { firstdaySun: true, autoCorrection: false },
};

export { Def };
