import { Func } from '../config';
import { ACTYPS } from './types';

export const dateSetToday = () => {
  return {
    type: ACTYPS.DATES.SETODAY,
    payload: { today: Func.getToday(), tomorrow: Func.getDay(1), yesterday: Func.getDay(-1) }
  };
};

export const dateSetCurrent = (date) => {
  return {
    type: ACTYPS.DATES.SETCURRENT,
    payload: date
  };
};

export const dateSetAvailable = (date) => {
  return {
    type: ACTYPS.DATES.SETAVAILABLE,
    payload: date
  };
};

export const dateSetAvailableToCurrent = () => {
  return {
    type: ACTYPS.DATES.SETAVAILABLETOCURRENT
  };
};

export const dateJump = (date) => {
  return {
    type: ACTYPS.DATES.JUMP,
    payload: date
  };
};

export const dateJumpEnd = () => {
  return {
    type: ACTYPS.DATES.JUMPEND
  };
};

export const dateReset = () => {
  return {
    type: ACTYPS.DATES.RESET
  };
};

export const dateSetAnim = (bool) => {
  return {
    type: ACTYPS.DATES.SETANIM,
    payload: bool
  };
};
