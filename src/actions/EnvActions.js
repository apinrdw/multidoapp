import { Platform, Dimensions } from 'react-native';
import { ACTYPS } from './types';


export const setEnvironment = (platf) => {
  const window = Dimensions.get('window');

  const env = {
    platf,
    os: Platform.OS,
    isdesktop: null,
    ismobile: null,
    isios: false,
    isandroid: false,
    width: window.width,
    height: window.height,
    wide: false,
    thin: false,
  };

  if (platf === 'desktop') { env.isdesktop = true; env.ismobile = false; }
  if (platf === 'mobile') { env.isdesktop = false; env.ismobile = true; }
  if (Platform.OS === 'ios') { env.isios = true; env.isandroid = false; }
  if (Platform.OS === 'android') { env.isios = false; env.isandroid = true; }
  if (window.width > 500) { env.wide = true; }
  if (window.height < 400) { env.thin = true; }

  return {
    type: ACTYPS.ENV.SET,
    payload: env
  };
};

export const setDesktopHeader = (platf, type, data) => {
  if (platf === 'desktop') {
    switch (type) {
      case 'icon': document.getElementById('appLogo').className = data; break;
      case 'header': document.getElementById('title-bar').className = document.getElementById('title-bar').className+' '+data; break;
      default: break;
    }
  }
};
