import { ACTYPS } from './types';

export const taskFormDateChanged = (date) => {
  return {
    type: ACTYPS.TASK.DATECHANGED,
    payload: date
  };
};

export const taskFormTitleChanged = (text) => {
  return {
    type: ACTYPS.TASK.TITLECHANGED,
    payload: text
  };
};

export const taskFormTitleErrorChanged = (text) => {
  return {
    type: ACTYPS.TASK.TITLEERROR,
    payload: text
  };
};

export const taskSetSum = ({ currentsum, donesum, date }) => {
  return {
    type: ACTYPS.TASK.SETSUM,
    payload: { currentsum, donesum, date }
  };
};

export const taskDateSet = (date) => {
  return {
    type: ACTYPS.TASK.SETDATE,
    payload: date
  };
};

export const taskFormSet = ({ title, date, taskdatas }) => {
  return {
    type: ACTYPS.TASK.SETFORM,
    payload: { title, date, taskdatas }
  };
};

export const taskFormReset = () => {
  return {
    type: ACTYPS.TASK.RESETFORM
  };
};

export const taskSetSortable = (bool) => {
  return {
    type: ACTYPS.TASK.SORTABLELIST,
    payload: bool
  };
};
