import React, { Component } from 'react';
import { View, AppState, NetInfo, Platform, UIManager } from 'react-native';
import { connect } from 'react-redux';
import isReachable from 'is-reachable'; // TODO: ignore this line when make web
import { userLoggedIn, setEnvironment, setDesktopHeader, todoFetch, dateSetToday, dateSetCurrent, dateSetAvailable, dateJump, notificationAdd, todoSettings, todoVisited, modalHowtoSet, modalHowtoTypeSet } from './actions';
import { Func, Def, Txt, Vstd } from './config';
import Logout from './components/modals/logout';
import Settings from './components/modals/settings';
import Howto from './components/modals/howto';
import TaskForm from './components/modals/TaskForm';
import AuthForms from './components/auth/AuthForms';
import AppLayout from './components/AppLayout';
import NotificationBar from './components/NotificationBar';
import { Loader } from './components/common';
import { Tracker, Gac } from './ga';

//const isReachable = null; // TODO: ignore this line when make app


// set LayoutAnimation on android
if (Platform.OS === 'android') { UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true); }

class Stage extends Component {

  state = {
    appState: AppState.currentState,
    isConnected: null,
  };

  componentWillMount() {
    Tracker.appAction(Gac.appact.open);
    this.props.setEnvironment(this.props.platform);
    this.props.userLoggedIn();
    setDesktopHeader(this.props.platform, 'header', 'visible');
  }

  componentDidMount() {
    AppState.addEventListener('change', this.handleAppStateChange.bind(this));
    this.startConnectivity();
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (nextProps.date.today !== null && nextProps.date.current == null) return true;
    if (!nextProps.todo.started && nextProps.loggedin) return true;
    if (!this.props.todo.received && nextProps.todo.received) return true;

    if (this.props.modal.logoutVisible !== nextProps.modal.logoutVisible) return true;
    if (this.props.modal.settingsVisible !== nextProps.modal.settingsVisible) return true;
    if (this.props.modal.taskformVisible !== nextProps.modal.taskformVisible) return true;
    if (this.props.modal.curtainVisible !== nextProps.modal.curtainVisible) return true;
    if (this.props.modal.howtoVisible !== nextProps.modal.howtoVisible) return true;

    if (this.props.loggedin !== nextProps.loggedin) return true;
    if (this.state.appState !== nextState.appState) return true;
    if (this.state.isConnected !== nextState.isConnected) return true;

    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.state.isConnected !== nextState.isConnected) {
      if (this.state.isConnected === null && !nextState.isConnected) {
        this.props.notificationAdd({ text: Txt.notification.nonet, type: 'nonet' });
      } else if (this.state.isConnected && !nextState.isConnected) {
        this.props.notificationAdd({ text: Txt.notification.nonet, type: 'nonet' });
      } else if (this.state.isConnected === false && nextState.isConnected) {
        this.props.notificationAdd({ text: Txt.notification.yesnet, type: 'yesnet' });
      }
    }

    if (nextProps.date.today !== null && nextProps.date.current == null) {
      this.props.dateSetCurrent(nextProps.date.today);
      this.props.dateSetAvailable(nextProps.date.today);
    }

    if (!nextProps.todo.started && nextProps.loggedin) {
        this.props.dateSetToday();
        this.props.todoFetch();
    }

    if (!this.props.todo.received && nextProps.todo.received) {
      this.setState({ isConnected: true });
      console.log('Data - received', Func.memorySizeOf(nextProps.todo.datas));
      Tracker.appAction(Gac.appact.dataloaded);

      if (!Vstd.check(nextProps.todo.visited, this.props.todoVisited)) {
        this.props.modalHowtoTypeSet(true);
        this.props.modalHowtoSet(true);
      }

      if (nextProps.todo.settings === null) {
        this.props.todoSettings(Def.settings);
      }
    }

    if (this.state.appState !== nextState.appState) {
      if (nextState.appState === 'active') {
        this.checkToday();
      }
    }

  }

  handleAppStateChange(appState) {
    this.setState({ appState });
  }

  startConnectivity() {
    switch (Platform.OS) {
      case 'ios':
        NetInfo.isConnected.fetch().done((isConnected) => {
          NetInfo.isConnected.addEventListener('change', this.handleConnectivityChange.bind(this));
        });
        break;
      case 'android':
        NetInfo.isConnected.fetch().done((isConnected) => {
          this.handleConnectivityChange(isConnected);
          NetInfo.isConnected.addEventListener('change', this.handleConnectivityChange.bind(this));
        });
         break;
      default:
        if (isReachable !== null) {
          isReachable('app.multido.net').then(reachable => {
            this.handleConnectivityChange(reachable);
            NetInfo.isConnected.addEventListener('change', this.handleConnectivityChange.bind(this));
          });
        }
    }
  }

  handleConnectivityChange(isConnected) {
    this.setState({ isConnected });
  }

  checkToday() {
    if (this.props.date.today !== Func.getToday()) {
      this.props.dateSetToday();
      if (this.props.date.current !== Func.getToday()) this.props.dateJump(Func.getToday());
    }
  }

  renderContent() {
    switch (this.props.loggedin) {
      case true: return this.renderApp();
      case false: return <AuthForms platform={this.props.platform} />;
      default: return <Loader />;
    }
  }

  renderApp() {
    if (this.props.todo.received) {
      return <AppLayout platform={this.props.platform} />;
    }

    return <Loader />;
  }

  renderTop() {
    if (this.props.env.isdesktop) {
      return <View className="topMoveSpace" />;
    }

    return null;
  }

  renderCurtain() {
    if (this.props.modal.curtainVisible) {
      return <View style={styles.curtain} />;
    }

    return null;
  }

  renderSettings() {
    if (this.props.loggedin && this.props.todo.settings !== null) {
      return <Settings visible={this.props.modal.settingsVisible} childrenUpdate />;
    }
    return null;
  }

  render() {
    return (
        <View style={styles.rootView}>
          {this.renderContent()}
          {this.renderTop()}
          {this.renderCurtain()}
          <TaskForm visible={this.props.modal.taskformVisible} />
          <Logout visible={this.props.modal.logoutVisible} />
          <Howto visible={this.props.modal.howtoVisible} firsttime={this.props.modal.howtoIsFirst} childrenUpdate />
          {this.renderSettings()}
          <NotificationBar />
        </View>
    );
  }
}

const styles = {
  rootView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'stretch',
    backgroundColor: Def.colorBlue
  },
  curtain: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    //bottom: 50,
    height: Def.stageHeight,
    backgroundColor: Def.colorWhite
  }
};

const mapStateToProps = ({ auth, env, todo, date, modal }) => {
  const { loggedin, error } = auth;
  return { loggedin, error, env, todo, date, modal };
};

export default connect(mapStateToProps, {
  userLoggedIn, setEnvironment, todoFetch, dateSetToday, dateSetCurrent, dateSetAvailable, dateJump, notificationAdd, todoSettings, todoVisited, modalHowtoSet, modalHowtoTypeSet
})(Stage);
