import { ACTYPS } from '../actions/types';

const INITIAL_STATE = {
  today: null,
  tomorrow: null,
  yesterday: null,
  current: null,
  available: null,
  anim: false,
  jump: null,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case ACTYPS.DATES.SETODAY:
        return { ...state, today: action.payload.today, tomorrow: action.payload.tomorrow, yesterday: action.payload.yesterday };
      case ACTYPS.DATES.SETCURRENT:
        return { ...state, current: action.payload };
      case ACTYPS.DATES.SETAVAILABLE:
        return { ...state, available: action.payload };
      case ACTYPS.DATES.SETAVAILABLETOCURRENT:
        return { ...state, current: state.available };
      case ACTYPS.DATES.JUMP:
        return { ...state, jump: action.payload };
      case ACTYPS.DATES.JUMPEND:
        return { ...state, jump: null };
      case ACTYPS.DATES.SETANIM:
        return { ...state, anim: action.payload };
      case ACTYPS.DATES.RESET:
        return INITIAL_STATE;
      default:
        return state;
    }
};
