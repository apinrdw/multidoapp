import React, { Component } from 'react';
import { Platform, View, Text, TouchableOpacity, Animated } from 'react-native';
import { Def, Txt } from '../../config';
import { Button, Input, ButtonLoad } from '../common';

class Forgotten extends Component {

  renderButton() {
    if (this.props.loading) return <ButtonLoad />;
    return <Button onPress={this.props.pressButton}>{Txt.forgottenpass.button}</Button>;
  }

  render() {
    return (
      <Animated.View style={[styles.container, this.props.rotation]} className="auth-container forgotten">
        <View style={[styles.section, styles.head]}>
          <Text style={styles.headText}>{Txt.forgottenpass.title.Break()}</Text>
        </View>
        <View style={styles.section}>
          <Input
            onChangeText={this.props.emailChangeText}
            value={this.props.emailValue}
            autoCapitalize="none"
            keyboardType="email-address"
            placeholder={Txt.forgottenpass.email}
            errorText={this.props.emailError}
            onSubmitEditing={this.props.onSubmitEditing}
            type={this.props.type}
          />
        </View>
        <View style={[styles.section, styles.foot]}>
          {this.renderButton()}
        </View>

        <View style={styles.extra}>
          <View style={styles.cancelspace} />
          <TouchableOpacity style={styles.cancel} onPress={this.props.pressRight}>
            <Text style={styles.cancelText}>{Txt.forgottenpass.link}</Text>
          </TouchableOpacity>
        </View>

      </Animated.View>
    );
  }
}

const styles = {
  container: {
    paddingTop: (Platform.OS === 'ios') ? 42 : 32,
    paddingLeft: 20,
    paddingRight: 20,
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    alignSelf: Def.wide ? 'stretch' : 'flex-start',
  },
  section: {
    flexDirection: 'row',
    paddingBottom: 0
  },
  head: {
    paddingBottom: 12
  },
  foot: {
    paddingTop: 16
  },
  headText: {
    fontFamily: Def.fontExtLight,
    fontSize: 30,
    color: Def.colorWhite
  },
  extra: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    paddingTop: 20,
  },
  cancelspace: {
    flex: 1,
  },
  cancel: {
    height: 40,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  cancelText: {
    fontFamily: Def.fontMedium,
    color: Def.colorWhite,
    fontSize: 14,
  },
};

export default Forgotten;
