import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Text, View, Animated, Easing } from 'react-native';
import { notificationAdd, notificationRemove } from '../actions';
import { Def } from '../config';

class NotificationBar extends Component {

  state = {
    translateY: new Animated.Value(-190),
    animDuration: 500,
    animDurationAutoBack: 1500,
    errorBounce: 2,
    delayBack: 3000,
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.notification.text !== nextProps.notification.text) return true;
    if (this.props.notification.type !== nextProps.notification.type) return true;
    return false;
  }

  componentDidUpdate() {
    if (this.props.notification.text !== null) {
      if (this.props.notification.type === 'nonet') {
        this.notificationShow();
      } else if (this.props.notification.type === 'yesnet') {
        this.notificationHide();
      } else {
        this.notificationShow(() => { this.notificationHide(); });
      }
    } else {
      Animated.timing(this.state.translateY, { toValue: -190, duration: 1 }).start();
    }
  }

  notificationShow(callback = null) {
    let topval = -90;
    if (this.props.env.isios) topval = -60;
    if (this.props.env.isandroid) topval = -65;
    Animated.parallel([
      Animated.timing(this.state.translateY, { toValue: topval, easing: Easing.out(Easing.back(this.state.errorBounce)), duration: this.state.animDuration }),
    ]).start(() => {
      if (callback !== null) callback();
    });
  }

  notificationHide(callback = null) {
    Animated.sequence([
        Animated.delay(this.state.delayBack),
        Animated.timing(this.state.translateY, { toValue: -190, easing: Easing.out(Easing.back(this.state.errorBounce)), duration: this.state.animDurationAutoBack }),
    ]).start(() => {
      this.props.notificationRemove();
      if (callback !== null) callback();
    });
  }

  renderErrorStyle() {
    const { translateY } = this.state;
    const animStyle = { transform: [{ translateY }]  };
    let backgroundColor = Def.colorBlack;
    switch (this.props.notification.type) {
      case 'error': backgroundColor = Def.colorRed; break;
      case 'nonet': backgroundColor = Def.colorRed; break;
      case 'info': backgroundColor = Def.colorInfo; break;
      case 'yesnet': backgroundColor = Def.colorInfo; break;
      default: backgroundColor = Def.colorBlack;

    }
    return [styles.container, animStyle, { backgroundColor }];
  }

  render() {
    return (
      <Animated.View style={this.renderErrorStyle()}>
        <Text style={styles.text}>{this.props.notification.text}</Text>
      </Animated.View>
    );
  }

}

const styles = {
  container: {
    position: 'absolute',
    zIndex: 500,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    top: 0,
    left: 0,
    right: 0,
    height: 180, //90,
  },
  text: {
    fontFamily: Def.fontRegular,
    color: Def.colorWhite,
    fontSize: 18,
    paddingLeft: 20,
    paddingRight: 20,
    paddingBottom: 16,
    textAlign: 'right',
  }
};

const mapStateToProps = ({ notification, env }) => {
  return { notification, env };
};

export default connect(mapStateToProps, {
  notificationAdd, notificationRemove
})(NotificationBar);
