import React, { Component } from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import { Def, Txt } from '../../config';
import Icon from './Icons';
import { CarouselItem } from './';

class Carousel extends Component {

  state = {
    currentItem: 0,
    nextItem: 0,
    clickable: true,
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.currentItem !== nextState.currentItem) return true;
    if (this.state.nextItem !== nextState.nextItem) return true;
    if (this.state.clickable !== nextState.clickable) return true;
    if (this.props.animation !== nextProps.animation) return true;
    if (this.props.firsttime !== nextProps.firsttime) return true;
    return false;
  }

  pressPrev() {
    if (this.state.clickable) {
      if (this.state.nextItem === 0) {
        this.setState({ nextItem: (this.props.items.length - 1), clickable: false });
      } else {
        this.setState({ nextItem: this.state.nextItem - 1, clickable: false });
      }
    }
  }

  pressNext() {
    if (this.state.clickable) {
      if (this.props.firsttime) {
        if (this.state.currentItem === (this.props.items.length - 1)) {
          this.props.endofFirst();
        }
      }

      if (this.state.nextItem >= (this.props.items.length - 1)) {
        this.setState({ nextItem: 0, clickable: false });
      } else {
        this.setState({ nextItem: this.state.nextItem + 1, clickable: false });
      }
    }
  }

  onAnimationOutEnd() {
    this.setState({ currentItem: this.state.nextItem });
  }

  onAnimationInEnd() {
    this.setState({ clickable: true });
  }

  renderDotts() {
    return this.props.items.map((item, key) =>
      <View style={[styles.dott, { opacity: (key === this.state.currentItem) ? 0.3 : 0.07 }]} key={key} />
    );
  }

  renderPrev() {
    if (this.props.firsttime) {
      if (this.state.currentItem > 0) {
        return (
          <TouchableOpacity style={styles.prev} onPress={this.pressPrev.bind(this)} activeOpacity={this.state.clickable ? 0.6 : 1} className="carousel-prev">
            <Icon src="left" fill={Def.colorIcon} />
          </TouchableOpacity>
        );
      }
      return <View style={styles.prev} />;
    }

    return (
      <TouchableOpacity style={styles.prev} onPress={this.pressPrev.bind(this)} className="carousel-prev">
        <Icon src="left" fill={Def.colorIcon} />
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.parent}>
          <CarouselItem
            data={this.props.items[this.state.currentItem]}
            length={this.props.items.length}
            currentItem={this.state.currentItem}
            nextItem={this.state.nextItem}
            animation={this.props.animation}
            onAnimationOutEnd={this.onAnimationOutEnd.bind(this)}
            onAnimationInEnd={this.onAnimationInEnd.bind(this)}
          />
        </View>

        <View style={styles.nav} className="carousel-nav">
          {this.renderPrev()}

          <View style={styles.dottcont} className="carousel-dotts">
            {this.renderDotts()}
          </View>

          <TouchableOpacity style={styles.next} onPress={this.pressNext.bind(this)} activeOpacity={this.state.clickable ? 0.6 : 1} className="carousel-next">
            <Text style={styles.nextText} className="carousel-next-text">
              {this.props.firsttime ? Txt.howto.continue : Txt.howto.next}
            </Text>
            <Icon src="right" fill={Def.colorBlue} />
          </TouchableOpacity>
        </View>

      </View>
    );
  }
}

const styles = {
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  parent: {
    height: 420,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'stretch',
  },
  nav: {
    marginTop: 40,
    height: 30,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  prev: {
    width: 110,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  next: {
    width: 110,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  nextText: {
    fontFamily: Def.fontMedium,
    color: Def.colorBlue,
    fontSize: 15,
    marginTop: -1,
    paddingRight: 0,
  },

  dottcont: {
    width: 60,
    height: 9,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },

  dott: {
    width: 6,
    height: 6,
    marginLeft: 3,
    marginRight: 3,
    borderRadius: 3,
    opacity: 0.1,
    backgroundColor: Def.colorBlack,
  }

};

export { Carousel };
