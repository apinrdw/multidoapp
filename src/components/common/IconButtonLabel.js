import React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { Def } from '../../config';
import Icon from './Icons';

const IconButtonLabel = (props) => {
  let iconColor = Def.colorBlue;
  switch (props.type) {
    case '': break;
    case 'black'     : iconColor = Def.colorBlack; break;
    case 'blue'      : iconColor = Def.colorBlue;  break;
    case 'ctx-blue'  : iconColor = Def.colorBlue;  break;
    case 'ctx-red'   : iconColor = Def.colorRed;   break;
    case 'red'       : iconColor = Def.colorRed;   break;
    case 'gray'      : iconColor = Def.colorIcon;  break;
    default          : iconColor = Def.colorBlue;
  }

  let labelColor = Def.colorTextGray;
  switch (props.type) {
    case 'black'     :   labelColor = Def.colorBlack; break;
    case 'blue'      :   labelColor = Def.colorBlue;  break;
    case 'ctx-blue'  :   labelColor = Def.colorBlack; break;
    case 'ctx-red'   :   labelColor = Def.colorBlack; break;
    default          :   labelColor = Def.colorTextGray;
  }

  let className = '';
  if (props.ctx) { className = 'ctxMenuItem'; }

  return (
    <View style={props.ctx ? styles.viewCtx : styles.view} className={className}>
      <TouchableOpacity onPress={props.onPress} activeOpacity={0.6} style={props.ctx ? [styles.button, styles.buttonCtx] : styles.button} >
        <Icon src={props.icon} fill={iconColor} today={props.today} />
        <Text style={[styles.label, { color: labelColor }]} className="icon-label">{props.label}</Text>
      </TouchableOpacity>
    </View>
  );
};

const styles = {
  button: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    borderRadius: 0,
    borderWidth: 0,
  },
  buttonCtx: {
    paddingLeft: 10
  },
  label: {
    fontFamily: Def.fontLight,
    color: Def.colorTextGray,
    fontSize: 15,
    paddingLeft: 6,
  },
  view: {
    height: 30,
    marginBottom: 4,
  },
  viewCtx: {
    height: 40,
    marginBottom: 0,
    borderBottomColor: Def.colorWhite,
    borderBottomWidth: 1
  }
};

export { IconButtonLabel };
