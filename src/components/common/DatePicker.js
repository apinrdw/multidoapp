import React, { Component } from 'react';
import c from 'calendar';
import { TouchableOpacity, View, Text, Animated, Easing } from 'react-native';
import { IconButton } from './';
import { Txt, Def } from '../../config';

class DatePicker extends Component {

  state = {
    year: null,
    month: null,
    day: null,
    first: null,
    current: null,
    pressed: null,
    monthdata: null,
    clickable: true,
    show: false,
    opacity: new Animated.Value(1),
    translateX: new Animated.Value(0),
    animDuraton: 250,
    animDistance: 50,
  };

  componentWillMount() {
    this.generateMonth(this.props, this.state);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.selectedDate !== nextProps.selectedDate) return true;
    if (this.props.today !== nextProps.today) return true;
    if (this.props.firstdaySun !== nextProps.firstdaySun) return true;
    if (this.props.animating !== nextProps.animating) return true;

    if (this.state.year !== nextState.year) return true;
    if (this.state.month !== nextState.month) return true;
    if (this.state.day !== nextState.day) return true;
    if (this.state.first !== nextState.first) return true;
    if (this.state.current !== nextState.current) return true;
    if (this.state.pressed !== nextState.pressed) return true;

    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (nextState.pressed === null) {
      if (!nextState.show) {
        this.generateMonth(nextProps, nextState);
      } else {
        this.setState({ show: false });
      }
    }

    if (this.props.selectedDate !== nextProps.selectedDate) {
      this.setState({ pressed: null });
    }
  }

  generateMonth(props, state) {
    const { firstdaySun, selectedDate } = props;
    const date = new Date(selectedDate);
    const year = date.getFullYear();
    const month = date.getMonth() + 1;
    const day = date.getDate();
    const m = (month < 10) ? `0${month}` : month;
    const d = (day < 10) ? `0${day}` : day;
    const first = firstdaySun ? 0 : 1;
    const cal = new c.Calendar(first);
    const monthdata = cal.monthDays(year, month - 1);
    this.setState({ monthdata, year, month, day, first, current: `${year}-${m}-${d}` });
  }

  generateDate(day) {
    const y = this.state.year;
    const m = (this.state.month < 10) ? `0${this.state.month}` : this.state.month;
    const d = (day < 10) ? `0${day}` : day;
    return `${y}-${m}-${d}`;
  }

  showMonth(step) {
    const { year, month, first } = this.state;
    const m = (step > 0) ? ((month < 12) ? month + step : 1) : ((month > 1) ? month + step : 12);
    const y = (step > 0) ? ((m === 1) ? year + step : year) : ((m === 12) ? year + step : year);
    const cal = new c.Calendar(first);
    const monthdata = cal.monthDays(y, m - 1);
    this.setState({ month: m, year: y, monthdata, show: true });
  }

  datePress(date) {
    if (!this.props.animating) {
      this.setState({ pressed: date });
      this.props.dateSelected(date);
    }
  }

  nextMonth() {
    if (this.state.clickable) {
      this.setState({ clickable: false });
      Animated.parallel([
        Animated.timing(this.state.opacity, { toValue: 0, duration: this.state.animDuraton }),
        Animated.timing(this.state.translateX, { toValue: -this.state.animDistance, duration: this.state.animDuraton }),
      ]).start(() => {
        this.showMonth(1);
        this.state.translateX.setValue(this.state.animDistance);
        Animated.parallel([
          Animated.timing(this.state.opacity, { toValue: 1, duration: this.state.animDuraton }),
          Animated.timing(this.state.translateX, { toValue: 0, duration: this.state.animDuraton }),
        ]).start(() => {
          this.setState({ clickable: true });
        });
      });
    }
  }

  prevMonth() {
    if (this.state.clickable) {
      this.setState({ clickable: false });
      Animated.parallel([
        Animated.timing(this.state.opacity, { toValue: 0, duration: this.state.animDuraton }),
        Animated.timing(this.state.translateX, { toValue: this.state.animDistance, duration: this.state.animDuraton }),
      ]).start(() => {
        this.showMonth(-1);
        this.state.translateX.setValue(-this.state.animDistance);
        Animated.parallel([
          Animated.timing(this.state.opacity, { toValue: 1, duration: this.state.animDuraton }),
          Animated.timing(this.state.translateX, { toValue: 0, duration: this.state.animDuraton }),
        ]).start(() => {
          this.setState({ clickable: true });
        });
      });
    }
  }

  renderBox(day) {
    if (day === 0) return null;
    const date = this.generateDate(day);
    const todaystyle = (this.props.today === date) ? styles.today : null;
    let currentstyle = null; let currenttextstyle = null;

    if (this.state.pressed === date) {
      currentstyle = styles.current; currenttextstyle = styles.currenttext;
    }

    if (this.state.pressed === null && this.state.current === date) {
      currentstyle = styles.current; currenttextstyle = styles.currenttext;
    }

    return (
      <TouchableOpacity onPress={() => this.datePress(date)} style={[styles.datecont, todaystyle, currentstyle]} >
        <Text style={[styles.text, currenttextstyle]} className="DatePickerDate">{`${day}`}</Text>
      </TouchableOpacity>
    );
  }

  renderCell(week) {
    return week.map((day, key) => <View style={styles.cell} key={key} className="DatePickerCell" data-type="calendar" data-dropdate={this.generateDate(day)}>{this.renderBox(day)}</View>);
  }

  renderRow() {
    return this.state.monthdata.map((week, key) => <View style={styles.row} key={key} className="DatePickerRow">{this.renderCell(week)}</View>);
  }

  renderHead() {
    const days = this.state.first ? Txt.calendar.weekdayshort : Txt.calendar.weekdayshortsun;
    return days.map((day, key) => <View style={styles.cell} key={key} className="DatePickerCell">
      <View style={styles.datecont}>
        <Text style={styles.text}>{day}</Text>
      </View>
    </View>);
  }

  renderBardate() {
    const month = Txt.calendar.monthnames[this.state.month - 1];
    return <Animated.Text style={[styles.bardate, this.renderAnimStyle()]}>{`${month} ${this.state.year}`}</Animated.Text>;
  }

  renderAnimStyle() {
    const { opacity, translateX } = this.state;
    return { opacity, transform: [{ translateX }] };
  }

  render() {
    return (
      <View style={[styles.container, (this.props.fixed ? styles.fixed : null)]} className="calendarContainer">
        <View style={styles.bar} className="DatePickerBar">
          <IconButton icon="left" onPress={this.prevMonth.bind(this)} color="#D5D4D4" ontouch />
          {this.renderBardate()}
          <IconButton icon="right" onPress={this.nextMonth.bind(this)} color="#D5D4D4" ontouch />
        </View>
        <Animated.View style={[styles.head, this.renderAnimStyle()]}>
          {this.renderHead()}
        </Animated.View>
        <Animated.View style={[styles.month, this.renderAnimStyle()]}>
          {this.renderRow()}
        </Animated.View>
      </View>
    );
  }

}

const styles = {
  container: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'stretch',
    paddingLeft: 20,
    paddingRight: 20,
  },
  fixed: {
    width: 300,
    height: 300,
  },
  bar: {
    height: 32,
    marginTop: 4,
    marginLeft: -10,
    marginRight: -10,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
  },
  head: {
    height: 26,
    marginTop: 10,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    flexDirection: 'row',
  },
  month: {
    marginLeft: -2,
    marginRight: -2,
  },
  row: {
    height: 26,
    marginTop: 4,
    justifyContent: 'space-between',
    alignItems: 'stretch',
    flexDirection: 'row',
  },
  cell: {
    flex: 1,
    marginLeft: 2,
    marginRight: 2,
    justifyContent: 'space-between',
    alignItems: 'stretch',
  },
  datecont: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  today: {
    borderWidth: 1,
    borderColor: Def.colorBlue,
  },
  current: {
    backgroundColor: Def.colorBlue
  },
  text: {
    fontFamily: Def.fontRegular,
    fontSize: 12,
    color: '#A8A6A6',
    alignSelf: 'center',
  },
  currenttext: {
    color: Def.colorWhite,
  },
  bardate: {
    flex: 1,
    fontFamily: Def.fontRegular,
    fontSize: 14,
    color: '#333333',
    alignSelf: 'center',
    textAlign: 'center',
  }

};

export { DatePicker };
