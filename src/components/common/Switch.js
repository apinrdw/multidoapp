import React, { Component } from 'react';
import $ from 'jquery';
import { TouchableOpacity, View, Text, Animated, Easing, Platform } from 'react-native';
import { Def } from '../../config';

class Switch extends Component {

  state = {
    trueWidth: 0,
    falseWidth: 0,
    padding: 16,
    opacity: 0.3,
    trueOpac: new Animated.Value(0.1),
    falseOpac: new Animated.Value(0.1),
    left: new Animated.Value(0),
    right: new Animated.Value(0),
    animDuration: 200,
  };

  componentDidMount() {
    this.setUnderline(this.props, this.state);
  }

  shouldComponentUpdate(nextProps, nextState) {
    if (this.state.trueWidth !== nextState.trueWidth) return true;
    if (this.state.falseWidth !== nextState.falseWidth) return true;
    if (this.props.value !== nextProps.value) return true;
    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.value !== nextProps.value) {
      let left = 0; let right = (nextState.falseWidth + nextState.padding); let trueOpac = 1; let falseOpac = nextState.opacity;
      if (!nextProps.value) { left = (nextState.trueWidth + nextState.padding); right = 0; trueOpac = nextState.opacity; falseOpac = 1; }

      Animated.parallel([
        Animated.timing(this.state.left, { toValue: 0, easing: Easing.out(Easing.back(this.state.errorBounce)), duration: this.state.animDuration }),
        Animated.timing(this.state.right, { toValue: 0, easing: Easing.out(Easing.back(this.state.errorBounce)), duration: this.state.animDuration }),
        Animated.timing(this.state.trueOpac, { toValue: this.state.opacity, duration: this.state.animDuration }),
        Animated.timing(this.state.falseOpac, { toValue: this.state.opacity, duration: this.state.animDuration }),
      ]).start(() => {
        Animated.parallel([
          Animated.timing(this.state.left, { toValue: left, easing: Easing.out(Easing.back(this.state.errorBounce)), duration: this.state.animDuration }),
          Animated.timing(this.state.right, { toValue: right, easing: Easing.out(Easing.back(this.state.errorBounce)), duration: this.state.animDuration }),
          Animated.timing(this.state.trueOpac, { toValue: trueOpac, duration: this.state.animDuration }),
          Animated.timing(this.state.falseOpac, { toValue: falseOpac, duration: this.state.animDuration }),
        ]).start();
      });
    }

    if (this.state.trueWidth !== nextState.trueWidth) { this.setUnderline(nextProps, nextState); }
    if (this.state.falseWidth !== nextState.falseWidth) { this.setUnderline(nextProps, nextState); }
  }

  onPress() {
    this.props.onValueChange(!this.props.value);
  }

  setUnderline(props, state) {
    const { opacity, padding, falseWidth, trueWidth } = state;
    const { value } = props;
    let left = 0; let right = (falseWidth + padding); let trueOpac = 1; let falseOpac = opacity;
    if (!value) { left = (trueWidth + padding); right = 0; trueOpac = opacity; falseOpac = 1; }
    this.state.left.setValue(left);
    this.state.right.setValue(right);
    this.state.trueOpac.setValue(trueOpac);
    this.state.falseOpac.setValue(falseOpac);
  }

  trueLayout(e) {
    if (Platform.OS === 'web') {
      setTimeout(() => {
        this.setState({ trueWidth: $(`[data-id="${this.props.dataId}"]`).find('.switchTrue').outerWidth() });
      }, 100);
    } else {
      this.setState({ trueWidth: e.nativeEvent.layout.width });
    }
  }

  falseLayout(e) {
    if (Platform.OS === 'web') {
      setTimeout(() => {
        this.setState({ falseWidth: $(`[data-id="${this.props.dataId}"]`).find('.switchFalse').outerWidth() });
      }, 50);
    } else {
      this.setState({ falseWidth: e.nativeEvent.layout.width });
    }
  }

  trueStyle() {
    return { opacity: this.state.trueOpac };
  }

  falseStyle() {
    return { opacity: this.state.falseOpac };
  }

  renderUnderline() {
    const sty = { left: this.state.left, right: this.state.right };
    return (
      <Animated.View style={[styles.underline, sty]} />
    );
  }

  render() {
    return (
      <View style={styles.wrapper}>

        <View style={styles.content} data-id={this.props.dataId}>
          <Animated.View style={[styles.labelcont, this.trueStyle()]} onLayout={this.trueLayout.bind(this)}>
            <Text style={styles.labeltext} className="switchTrue">{this.props.trueLabel}</Text>
          </Animated.View>
          <Animated.View style={[styles.labelcont, styles.last, this.falseStyle()]} onLayout={this.falseLayout.bind(this)}>
            <Text style={styles.labeltext} className="switchFalse">{this.props.falseLabel}</Text>
          </Animated.View>
        </View>

        {this.renderUnderline()}

        <View style={styles.touchview}>
          <TouchableOpacity  style={styles.touch} onPress={this.onPress.bind(this)} />
        </View>

      </View>
    );
  }
}

const styles = {
  wrapper: {
    height: 40,
  },
  touchview: {
    position: 'absolute',
    zIndex: 100,
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  touch: {
    flex: 1,
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
  },
  last: {
    marginLeft: 16,
  },
  labeltext: {
    fontFamily: Def.fontMedium,
    alignSelf: 'flex-end',
    color: Def.colorBlue,
    fontSize: 17,
  },

  underline: {
    position: 'absolute',
    zIndex: 50,
    bottom: 6,
    left: 0,
    right: 0,
    height: 1,
    backgroundColor: Def.colorBlue,
  }

};

export { Switch };
