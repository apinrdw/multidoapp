import React from 'react';
import { View } from 'react-native';
import { Loader } from './';
import { Def } from '../../config';

const ButtonLoad = () => {
  return (
    <View style={styles.view}>
      <Loader color='blue' />
    </View>
  );
};

const styles = {
  view: {
    flex: 1,
    backgroundColor: Def.colorGray,
    height: Def.buttonHeight,
  }
};

export { ButtonLoad };
