import React from 'react';
import { View, Text, Platform } from 'react-native';
import { Def, Txt } from '../../config';

const DayHeader = ({ date }) => {
  const jdate = new Date(date);
  const dayname = Txt.calendar.weekday[jdate.getDay()];
  const month = Txt.calendar.monthnamesshort[jdate.getMonth()];
  const day = (jdate.getDate() < 10) ? `0${jdate.getDate()}` : jdate.getDate();

  return (
    <View style={styles.view}>
      <View style={styles.dayview}>
        <Text style={styles.dayname}>{dayname}</Text>
      </View>
      <View style={styles.dateview}>
        <Text style={styles.month}>{month}.</Text>
        <Text style={styles.day}>{day}</Text>
      </View>
    </View>
  );
};

const styles = {
  view: {
    height: 80,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
  },
  dayview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    backgroundColor: Def.colorBlue,
  },
  dateview: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'flex-start',
    paddingBottom: (Platform.OS === 'android') ? 15 : 18,
  },
  dayname: {
    fontFamily: Def.fontLight,
    color: Def.colorWhite,
    fontSize: 24,
    alignSelf: 'flex-start',
    paddingBottom: 18
  },
  month: {
    fontFamily: Def.fontLight,
    color: Def.colorWhite,
    fontSize: 24,
    alignSelf: 'flex-end',
    paddingRight: 10,
    marginBottom: (Platform.OS === 'ios') ? -2 : 0,
  },
  day: {
    fontFamily: Def.fontExtLight,
    color: Def.colorWhite,
    fontSize: 64,
    alignSelf: 'flex-end',
    marginBottom: (Platform.OS === 'android') ? -13 : ((Platform.OS === 'ios') ? -11.5 : -10),
  }
};

export default DayHeader;
