import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Dimensions } from 'react-native';
import ClickOutHandler from 'react-onclickout';
import { ctxmenuHide } from '../../actions';
import { IconButtonLabel } from '../common';
import { Def, Txt } from '../../config';
import { Tracker, Gac } from '../../ga';

class CtxMenu extends Component {

  state = {
    width: 150,
    height: 80
  };

  shouldComponentUpdate(nextProps, nextState) {
    if (this.props.windowSize !== nextProps.windowSize) { return true; }
    if (this.props.ctxmenu.visible !== nextProps.ctxmenu.visible) { return true; }
    return false;
  }

  componentWillUpdate(nextProps, nextState) {
    if (this.props.windowSize !== nextProps.windowSize) {
      this.props.ctxmenuHide();
    }
  }

  onClickOut() {
    this.props.ctxmenuHide();
  }

  pressEdit() {
    this.props.ctxmenu.edit();
    Tracker.userAction(Gac.usract.rowctxedit);
  }

  pressDelete() {
    this.props.ctxmenu.remove();
    Tracker.userAction(Gac.usract.rowctxremove);
  }

  renderPosition() {
    const window = Dimensions.get('window');
    let top = this.props.ctxmenu.top;
    let left = this.props.ctxmenu.left;
    if (top > (window.height - this.state.height)) { top = top - this.state.height; }
    if (left > (window.width - this.state.width)) { left = left - this.state.width; }
    return { top, left };
  }

  render() {
    if (this.props.ctxmenu.visible) {
      Tracker.userAction(Gac.usract.rowctx);
      return (
        <View style={[styles.ctxMenuCont, this.renderPosition()]}>
          <ClickOutHandler onClickOut={this.onClickOut.bind(this)}>
            <View style={styles.insideOut} className="ctxMenuContainer">
              <IconButtonLabel icon="pen" label={Txt.ctx.edit} onPress={this.pressEdit.bind(this)} type="ctx-blue" ctx />
              <IconButtonLabel icon="bin" label={Txt.ctx.delete} onPress={this.pressDelete.bind(this)} type="ctx-red" ctx />
            </View>
          </ClickOutHandler>
        </View>
      );
    }

    return null;
  }
}

const styles = {
  ctxMenuCont: {
    zIndex: 2000,
    position: 'fixed',
    width: 150,
    top: 0,
    left: 0,
    shadowOffset: { width: 5, height: 5 },
    shadowColor: 'black',
    shadowOpacity: 0.15,
    shadowRadius: 30
  },
  insideOut: {
    flex: 1,
    backgroundColor: Def.colorGray,
  }
};

const mapStateToProps = ({ ctxmenu }) => {
  return { ctxmenu };
};

export default connect(mapStateToProps, { ctxmenuHide })(CtxMenu);
