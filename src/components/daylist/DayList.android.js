import React, { Component } from 'react';
import DayListMobile from './DayListMobile';

class DayList extends Component {
  render() {
    return <DayListMobile {...this.props} />;
  }
}

export default DayList;
