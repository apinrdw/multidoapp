import React, { Component } from 'react';
import TaskFormMobile from './TaskFormMobile';

class TaskForm extends Component {

  renderForm() {
    if (this.props.visible) {
      return <TaskFormMobile {...this.props} />;
    }

    return null;
  }

  render() {
    return this.renderForm();
  }
}

export default TaskForm;
