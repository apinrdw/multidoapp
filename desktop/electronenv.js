function multidoExternalLink(url) {
  const shell = require("electron").shell;
  shell.openExternal(url);
}

(function () {
    // Retrieve remote BrowserWindow
    const {BrowserWindow} = require('electron').remote;
    const os = require('os');


    function init() {
        const platform = os.platform();
        document.getElementById("title-bar").className = document.getElementById("title-bar").className+" OS-"+platform;


        // Minimize task
        document.getElementById("min-btn").addEventListener("click", (e) => {
            var window = BrowserWindow.getFocusedWindow();
            window.minimize();
        });

        // Maximize window
        document.getElementById("max-btn").addEventListener("click", (e) => {
            var window = BrowserWindow.getFocusedWindow();
            if(window.isMaximized()){
                window.unmaximize();
                document.getElementById("max-btn").className = "bar-btn";
            }else{
                window.maximize();
                document.getElementById("max-btn").className = "bar-btn maximized";
            }
        });

        // Close app
        document.getElementById("close-btn").addEventListener("click", (e) => {
            var window = BrowserWindow.getFocusedWindow();
            window.close();
        });


        // Minimize task
        document.getElementById("min-btn-osx").addEventListener("click", (e) => {
            var window = BrowserWindow.getFocusedWindow();
            window.minimize();
        });

        // Maximize window
        document.getElementById("max-btn-osx").addEventListener("click", (e) => {
            var window = BrowserWindow.getFocusedWindow();
            if(window.isMaximized()){
                window.unmaximize();
                document.getElementById("max-btn").className = "bar-btn";
            }else{
                window.maximize();
                document.getElementById("max-btn").className = "bar-btn maximized";
            }
        });

        // Close app
        document.getElementById("close-btn-osx").addEventListener("click", (e) => {
            var window = BrowserWindow.getFocusedWindow();
            window.close();
        });


        // Minimize task
        document.getElementById("min-btn-win").addEventListener("click", (e) => {
            var window = BrowserWindow.getFocusedWindow();
            window.minimize();
        });

        // Maximize window
        document.getElementById("max-btn-win").addEventListener("click", (e) => {
            var window = BrowserWindow.getFocusedWindow();
            if(window.isMaximized()){
                window.unmaximize();
                document.getElementById("max-btn-win").className = "bar-btn";
            }else{
                window.maximize();
                document.getElementById("max-btn-win").className = "bar-btn maximized";
            }
        });

        // Close app
        document.getElementById("close-btn-win").addEventListener("click", (e) => {
            var window = BrowserWindow.getFocusedWindow();
            window.close();
        });
    };

    document.onreadystatechange =  () => {
        if (document.readyState == "complete") {
            init();
        }
    };
})();

// Pretend that cookies work
(function (document) {
  var cookies = {};
  document.__defineGetter__('cookie', function () {
    var output = [];
    for (var cookieName in cookies) {
      output.push(cookieName + "=" + cookies[cookieName]);
    }
    return output.join(";");
  });
  document.__defineSetter__('cookie', function (s) {
    var indexOfSeparator = s.indexOf("=");
    var key = s.substr(0, indexOfSeparator);
    var value = s.substring(indexOfSeparator + 1);
    cookies[key] = value;
    return key + "=" + value;
  });
  document.clearCookies = function () {
    cookies = {};
  };

  // Pretend that we're hosted on an Internet Website
  document.__defineGetter__('location', function() {
    return {
      href: 'http://atom-shell.local/',
      protocol: 'http:',
      host: 'atom-shell.local',
      port: '',
      pathname: '/',
      search: '',
      hash: '',
      username: '',
      password: '',
      origin: 'http://atom-shell.local'
    };
  });

  // Nobody sets location
  document.__defineSetter__('location', function() {})
})(document);

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

//ga('create', 'UA-93672628-1', 'auto');
//ga('send', 'pageview');

var GA_LOCAL_STORAGE_KEY = 'ga:clientId';

ga('create', 'UA-93672628-1', 'auto', {
   'storage': 'none',
   'clientId': localStorage.getItem(GA_LOCAL_STORAGE_KEY)
});

ga(function(tracker) {
   localStorage.setItem(GA_LOCAL_STORAGE_KEY, tracker.get('clientId'));
});

ga('set', 'checkProtocolTask', null);
